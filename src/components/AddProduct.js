import { useState, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function AddProduct(){

    const navigate = useNavigate();
    const {user} = useContext(UserContext);

    const [name,setName] = useState("");
    const [description,setDescription] = useState("");
    const [price,setPrice] = useState("");
    const [quantity,setQuantity] = useState("");

    function createProduct(e){
        e.preventDefault();

        let token = localStorage.getItem('token');
        console.log(token);

        fetch('https://cpstn3-be-ecommerceapi-magpantay.onrender.com/products/',{

            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({

                name: name,
                description: description,
                price: price,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data){
                Swal.fire({
                    icon:"success",
                    title: "Product Successfully Added",
                    text: 'Start making waves now.'

                })

                navigate("/products");
            } else {
                Swal.fire({
                    title:'Oops...!',
                    icon:'error',
                    text:'Something went wrong!'
                })
            }

        })

        setName("")
        setDescription("")
        setPrice(0);
        setQuantity(0);
    }

  return user.isAdmin ? (
    <Container fluid>
    <div className="fullview">
    <div className="add-product-container">
      <div className="add-product">
      <h1 className="my-5 text-center">Add Product</h1>
      <Form onSubmit={(e) => createProduct(e)}>
        <Form.Group>
          <Form.Label className="add-product-font">Name:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Name"
            required
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label className="add-product-font">Description:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Description"
            required
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label className="add-product-font">Price:</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter Price"
            required
            value={price}
            onChange={(e) => {
              setPrice(e.target.value);
            }}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label className="add-product-font">Quantity:</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter Quantity"
            required
            value={quantity}
            onChange={(e) => {
              setQuantity(e.target.value);
            }}
          />
        </Form.Group>
        <div className="register-button-container">
        <Button type="submit" className="my-5 register-button">
          Submit
        </Button>
        </div>
      </Form>
      </div>
        </div>
          </div>
    </Container>
  ) : (
    <Navigate to="/products" />
  );
}