import { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';


export default function AdminView({ productsData, fetchData }) {
    const [products, setProducts] = useState([]);

    const productImages = {
        "6502e81489a01d52c55c6655": "crocs.png",
        "6502eb1589a01d52c55c7823": "bucketHat.png",
        "6502ebb289a01d52c55c8154": "leatherWallet.png",
        "6504083675dd8a7dfd62b209": "balicomb.png",
        "65040c4b75dd8a7dfd62cd68": "yellowUmbrella.png",
        "65040c9f75dd8a7dfd62d08b": "gamingMouse.png"
    };

    useEffect(() => {
  const productsArr = productsData.map(product => {
    const imageFileName = productImages[product._id];
    const imageUrl = `/images/${imageFileName}`;

    return (
      <tr key={product._id}>
        <td>{product._id}</td>
        <td>{product.name}</td>
        <td>{product.description}</td>
        <td>{product.price}</td>
        <td>{product.quantity}</td>
        <td className={product.isActive ? "text-success" : "text-danger"}>
          {product.isActive ? "Available" : "Unavailable"}
        </td>
        <td className="text-center">
            <img
                src={imageUrl}
                alt={product.name}
                className="product-image"
            />
       </td>
        <td>
          <div className="d-flex flex-column">
            <div>
              <EditProduct product={product._id} fetchData={fetchData} />
            </div>
            <div className="mt-2">
              <ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData} />
            </div>
          </div>
        </td>
      </tr>
    );
  });

  setProducts(productsArr);
}, [productsData, productImages]);

    return (
<>
        <h1 className="text-center my-4 custom-h1">Product Masterlist</h1>
        <Table bordered hover responsive>
          <thead>
            <tr className="text-center">
              <th>ID</th>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Availability</th>
              <th>Product Images</th>
              <th colSpan="2">Product Actions</th>
            </tr>
          </thead>

          <tbody>{products}</tbody>
        </Table>
    </>
    );
}