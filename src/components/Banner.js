import { Button, Row, Col, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';

export default function Banner({data}) {

    console.log(data);
    const {title, content, destination, label} = data;

    return (
        <Row>
            <Col className="mt-5 text-center home-banner">
                <h1 className="my-4">{title}</h1>
                <p className="white-font my-4">{content}</p>
                <Link className="btn btn-warning my-4" to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}