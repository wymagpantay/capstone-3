import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';

const CartContent = () => {
  const [cart, setCart] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch('https://cpstn3-be-ecommerceapi-magpantay.onrender.com/carts', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => {
        console.log('Response Data:', data); 

        if (data.success) {
          setCart(data.cart);
        } else {
          console.error(data.message); 
        }
      })
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  // Calculate the total cart amount
  const totalAmount = cart.reduce((total, item) => total + item.productPrice * item.quantity, 0);

  return (
    <Container fluid>
      <div className="cart-container">
        <div className="cart-contents">
          <h1 className="p-3 text-center">My Cart</h1>
          {cart.length === 0 ? (
            <p className="text-center cart-contents-font">Your cart is empty.</p>
          ) : (
            <>
              <ul className="p-5">
                {cart.map((item) => (
                  <li key={item.productId}>
                    <p className="cart-contents-font">Product Name:<span className="bold-text"> {item.productName}</span></p>
                    <p className="cart-contents-font">Description:<span className="bold-text"> {item.productDescription}</span></p>
                    <p className="cart-contents-font">Price per Item:<span className="bold-text"> ${item.productPrice}</span></p>
                    <p className="cart-contents-font">Quantity:<span className="bold-text"> {item.quantity}</span></p>
                    <p className="cart-contents-font">Total Price:<span className="bold-text"> ${item.productPrice * item.quantity}</span></p>
                  </li>
                ))}
              </ul>
              <p className="text-center cart-contents-font">
                Total Amount of Items in Cart: <span className="bold-text">${totalAmount}</span>
              </p>
            </>
          )}
        </div>
      </div>
    </Container>
  );
};

export default CartContent;