import React, { useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Container } from 'react-bootstrap';

export default function Checkout() {
  const { user } = useContext(UserContext);

  const handleCheckout = () => {
    console.log('Checkout button clicked');

    fetch('https://cpstn3-be-ecommerceapi-magpantay.onrender.com/users/checkout-cart', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => {
        if (res.ok) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Cart checkout successful',
          });
          console.log('Checkout successful');
        } else {
          Swal.fire({
            title: 'Error',
            icon: 'error',
            text: 'Cart checkout failed. Please try again.',
          });
          console.error('Checkout failed');
        }
      })
      .catch((error) => {
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred during checkout. Please try again later.',
        });
        console.error('An error occurred during checkout', error);
      });
  };

  return (
    <div>
      {user.id !== null && (
        <div className="search-button-container">
        <button onClick={handleCheckout} className = "search-button">
          Checkout
        </button>
        </div>
      )}
    </div>
  );
}