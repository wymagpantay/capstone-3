import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';

function CurrentTime() {
  const [currentTime, setCurrentTime] = useState(new Date());

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentTime(new Date());
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);

  const formattedTime = currentTime.toLocaleTimeString();

  return (
    <Container fluid>
    <div className="text-center mt-2 clock">
      <div className="bg-warning text-white p-1 rounded">
        <p className="mb-0">Current Time:</p>
        <p className="mb-0" style={{ fontSize: '12px' }}>{formattedTime}</p>
      </div>
    </div>
    </Container>
  );
}

export default CurrentTime;