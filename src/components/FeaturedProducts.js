import { useState, useEffect } from 'react';
import { CardGroup, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PreviewProducts from './PreviewProducts'

export default function FeaturedProducts(){

	const [ previews, setPreviews ] = useState([]);

	useEffect(()=>{
		fetch(`https://cpstn3-be-ecommerceapi-magpantay.onrender.com/products/`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			const numbers = []
			const featured = []

		const generateRandomNums = () => {
			let randomNum = Math.floor(Math.random()*data.length)

			if(numbers.indexOf(randomNum) === -1){
				numbers.push(randomNum);
			}else{
				generateRandomNums()
			}
		}
		for(let i=0;i<data.length;i++){
			generateRandomNums()
			featured.push(<PreviewProducts data={data[numbers[i]]} key={data[numbers[i]]._id} breakPoint={2} />)
		}
		setPreviews(featured)
		})
	},[])

	return(
		<>
		<Container fluid>
		  <h2 className="text-center white-font my-5">Featured Products</h2>
		  <CardGroup className="justify-content-center">	
		    {previews}
		  </CardGroup>
		</Container>
		</>
	)
}