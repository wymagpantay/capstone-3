import { Row, Col, Card, Container } from 'react-bootstrap'


export default function Highlights() {

	return (
		<Container fluid>
		<h3 className="text-center white-font my-5">Be part of our growing community of shoppers and sellers!</h3>
		<Row className="mt-3 mb-3">
			<Col xs={12} s={6} md={2.5} lg={3}>
			  <Card className="service-card p-3">
			    <Card.Body>
			      <div className="d-flex align-items-center justify-content-center mb-3">
			        <div className="text-center"> 
			          <img src="/icons/returnIcon.png" alt="Icon 1" className="card-icon" />
			        </div>
			      </div>
			      <div className="text-center"> 
			        <Card.Title>
			          <h2>Free Delivery</h2>
			        </Card.Title>
			      </div>
			      <div className="text-center"> 
			        <Card.Text>
			          Swift shipping at your doorstep, with no extra cost. Enjoy the convenience of free delivery today.
			        </Card.Text>
			      </div>
			    </Card.Body>
			  </Card>
			</Col>
			<Col xs={12} s={6} md={2.5} lg={3}>
			  <Card className="service-card p-3">
			    <Card.Body>
			      <div className="d-flex align-items-center justify-content-center mb-3">
			        <div className="text-center"> 
			          <img src="/icons/returnIcon.png" alt="Icon 2" className="card-icon" />
			        </div>
			      </div>
			      <div className="text-center"> 
			        <Card.Title>
			          <h2>90 Days Return</h2>
			        </Card.Title>
			      </div>
			      <div className="text-center"> 
			        <Card.Text>
			          Return any purchase within 90 days hassle-free. Shop confidently with our extended return policy.
			        </Card.Text>
			      </div>
			    </Card.Body>
			  </Card>
			</Col>
			<Col xs={12} s={6} md={2.5} lg={3}>
			  <Card className="service-card p-3">
			    <Card.Body>
			      <div className="d-flex align-items-center justify-content-center mb-3">
			        <div className="text-center"> 
			          <img src="/icons/paymentIcon.png" alt="Icon 3" className="card-icon" />
			        </div>
			      </div>
			      <div className="text-center"> 
			        <Card.Title>
			          <h2>Secure Payment</h2>
			        </Card.Title>
			      </div>
			      <div className="text-center"> 
			        <Card.Text>
			          Enjoy secure payments for worry-free shopping. Your financial safety is our priority.
			        </Card.Text>
			      </div>
			    </Card.Body>
			  </Card>
			</Col>
			<Col xs={12} s={6} md={2.5} lg={3}>
			  <Card className="service-card p-3">
			    <Card.Body>
			      <div className="d-flex align-items-center justify-content-center mb-3">
			        <div className="text-center"> 
			          <img src="/icons/supportIcon.png" alt="Icon 4" className="card-icon" />
			        </div>
			      </div>
			      <div className="text-center"> 
			        <Card.Title>
			          <h2>24/7 Online Support</h2>
			        </Card.Title>
			      </div>
			      <div className="text-center"> 
			        <Card.Text>
			          Our team is here 24/7 to assist you, offering prompt solutions and peace of mind anytime you require it.
			        </Card.Text>
			      </div>
			    </Card.Body>
			  </Card>
			</Col>
		</Row>
		</Container>
	)
}