import React, { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';

const OrderView = () => {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch('https://cpstn3-be-ecommerceapi-magpantay.onrender.com/users/orders', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setOrders(data.orders);
      })
      .catch((error) => {
        console.error('Error fetching orders:', error);
      });
  }, []);
  return (
    <Container fluid>
    <div className="fullview">
    <div className="order-view">
      <h2>Order History</h2>
      {orders.map((order) => (
        <div key={order._id}>
          <h3>Product Name: {order.productName}</h3>
          <p>Date Ordered: {new Date(order.orderedOn).toLocaleDateString()}</p>
          <p>Status: {order.status}</p>
        </div>
      ))}
      </div>
      </div>
    </Container>
  );
};

export default OrderView;