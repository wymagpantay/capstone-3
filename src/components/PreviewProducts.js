import React from 'react';
import { Col, Card, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Product(props) {
  const productImages = {
    "6502e81489a01d52c55c6655": "crocs.png",
    "6502eb1589a01d52c55c7823": "bucketHat.png",
    "6502ebb289a01d52c55c8154": "leatherWallet.png",
    "6504083675dd8a7dfd62b209": "balicomb.png",
    "65040c4b75dd8a7dfd62cd68": "yellowUmbrella.png",
    "65040c9f75dd8a7dfd62d08b": "gamingMouse.png"
  };

  const { breakPoint, data } = props;
  const { _id, name, description, price } = data;

  const imageFileName = productImages[_id];
  const imageUrl = `/images/${imageFileName}`;

  return (
    <Col xs={12} sm={4} md={4} lg={4}>
    <div className="product-images-card">
      <Card className="cardHighlight mx-2">
        <Card.Body>
        <div className="center-image">
            <img src={imageUrl} alt={name} className="product-image" />
          </div>
          <Card.Title className="text-center">
            <Link to={`/products/${_id}`} className="link-style">{name}</Link>
          </Card.Title>
          <Card.Text>{description}</Card.Text>
        </Card.Body>

        <Card.Footer>
          <h5 className="text-center">P{price}</h5>
          <Link className="btn btn-warning d-block" to={`/products/${_id}`}>
            Details
          </Link>
        </Card.Footer>
      </Card>
      </div>
    </Col>
  );
}