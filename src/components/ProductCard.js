import { useState } from 'react';
import { Card, Button, Container } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
 
export default function ProductCard({productProp}) {

	const {_id, name, description, price} = productProp;

	console.log(useState(0))

	return (
		<Container fluid>
		<div className="product-container">
		<Card className="mt-3 product-form">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
		</div>
		</Container>
	)
}

ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

