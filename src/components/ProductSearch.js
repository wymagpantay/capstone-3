import React, { useState } from 'react';
import ProductCard from './ProductCard';
import Container from 'react-bootstrap/Container';

const ProductSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [showResults, setShowResults] = useState(false);

  const handleSearch = async () => {
    try {
      const response = await fetch('https://cpstn3-be-ecommerceapi-magpantay.onrender.com/products/searchByName', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
      setShowResults(true);
    } catch (error) {
      console.error('Error searching for products:', error);
    }
  };

  const handleDoAnotherSearch = () => {
    setSearchResults([]);
    setShowResults(false);
    setSearchQuery(''); 
  };

  return (
      <Container fluid>
      <div className="search-container">
        <div className='pt-5 search-form'>
          <h2>Search by Name:</h2>
          <div className="form-group">
            <label htmlFor="productName" className="search-form-font">Product Name:</label>
            <input
              type="text"
              id="productName"
              className="form-control"
              value={searchQuery}
              onChange={event => setSearchQuery(event.target.value)}
            />
          </div>
          {showResults ? ( // Check if showResults is true
            <>
              <div className="search-button-container">
              <button className = "search-button" onClick={handleDoAnotherSearch}>
                Do Another Search
              </button>
              </div>
              <h3>Search Results:</h3>
              <ul>
                {searchResults.map(product => (
                  <ProductCard productProp={product} key={product._id} />
                ))}
              </ul>
            </>
          ) : (
            <div className="search-button-container">
            <button className = "search-button" onClick={handleSearch}>
              Search Products
            </button>
            </div>
          )}
        </div>
        </div>
      </Container>
  );
};

export default ProductSearch;