import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import ProductCard from './ProductCard';

const SearchByPrice = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [products, setProducts] = useState([]);
  const [showResults, setShowResults] = useState(false);

  const handleMinPriceChange = (e) => {
    setMinPrice(e.target.value);
  };

  const handleMaxPriceChange = (e) => {
    setMaxPrice(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ minPrice, maxPrice }),
    };

    fetch('https://cpstn3-be-ecommerceapi-magpantay.onrender.com/products/searchByPrice', requestOptions)
      .then((response) => response.json())
      .then((data) => {
        setProducts(data.products);
        setShowResults(true);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  return (
    <Container fluid className="mt-5">
      <div className="search-container">
        <div className='pt-5 search-form'>
          <h2>Search Products by Price Range</h2>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="minPrice" className="search-form-font">
                Min Price:
              </label>
              <input
                type="number"
                className="form-control"
                id="minPrice"
                value={minPrice}
                onChange={handleMinPriceChange}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="maxPrice" className="search-form-font">
                Max Price:
              </label>
              <input
                type="number"
                className="form-control"
                id="maxPrice"
                value={maxPrice}
                onChange={handleMaxPriceChange}
              />
            </div>
            <div className="search-button-container">
            <button type="submit" className = "search-button">
              Search Products
            </button>
            </div>
          </form>
          {showResults && (
            <>
              <h3>Search Results:</h3>
              <div className="product-card-container">
                {/* Map through products and render ProductCard components */}
                {products.map((product) => (
                  <ProductCard productProp={product} key={product.id} />
                ))}
              </div>
            </>
          )}
        </div>
      </div>
    </Container>
  );
};

export default SearchByPrice;