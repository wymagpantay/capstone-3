import React, { useState, useContext } from 'react';
import { Row, Col, Form, Button, Container } from 'react-bootstrap';
import UserContext from '../UserContext';

const UpdateProfile = () => {
  const { user } = useContext(UserContext);
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    mobileNo: '',
  });
  const [message, setMessage] = useState(null);
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = localStorage.getItem('token');
    const requestOptions = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(formData),
    };

    try {
      const response = await fetch(`https://cpstn3-be-ecommerceapi-magpantay.onrender.com/users/profile`, requestOptions);
      const data = await response.json();

      if (response.ok) {
        setMessage('Profile updated successfully');
      } else {
        setMessage(data.message || 'Profile update failed');
      }
    } catch (error) {
      setMessage('An error occurred. Please try again.');
      console.error(error);
    }
  };

  return (
    <Container fluid>
    <div className="reset-password-container">
    <div className='pt-5 reset-password-form'>
    <Row>
      <Col>
        <h2 className="text-center">Edit Profile</h2>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="firstName">
            <Form.Label className="reset-password-font">First Name</Form.Label>
            <Form.Control
              type="text"
              name="firstName"
              value={formData.firstName}
              onChange={handleInputChange}
            />
          </Form.Group>
          <Form.Group controlId="lastName">
            <Form.Label className="reset-password-font">Last Name</Form.Label>
            <Form.Control
              type="text"
              name="lastName"
              value={formData.lastName}
              onChange={handleInputChange}
            />
          </Form.Group>
          <Form.Group controlId="mobileNo">
            <Form.Label className="reset-password-font">Mobile Number</Form.Label>
            <Form.Control
              type="text"
              name="mobileNo"
              value={formData.mobileNo}
              onChange={handleInputChange}
            />
            {message && <div className={`mt-2 alert ${message.includes('successfully') ? 'alert-success' : 'alert-danger'}`}>{message}</div>}
          </Form.Group>
          <div className="reset-button-container">
          <Button type="submit" className = "search-button">Save Changes</Button>
          </div>
        </Form>
      </Col>
    </Row>
    </div>
    </div>
    </Container>
  );
};

export default UpdateProfile;