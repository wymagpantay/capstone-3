import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch'
import SearchByPrice from './SearchByPrice'
import { Container } from 'react-bootstrap';
import FeaturedProducts from './FeaturedProducts'

export default function UserView({productsData}) {

    const [products, setProducts] = useState([])

    useEffect(() => {
        const productsArr = productsData.map(product => {

            if(product.isActive === true) {
                return (
                    <ProductCard productProp={product} key={product._id}/>
                    )
            } else {
                return null;
            }
        })
        setProducts(productsArr)

    }, [productsData])

    return(
        <>
            <ProductSearch/>
            <SearchByPrice/>
            <FeaturedProducts />
        </>
        )
}
