import React from 'react';
import CartContent from '../components/CartContent';
import Checkout from '../components/Checkout'
import { Container } from 'react-bootstrap';


const Cart = () => {
  return (
    <Container fluid>
    <div className="fullview">
      <CartContent />
      <Checkout />
    </div>
    </Container>
  );
};

export default Cart;