import Banner from '../components/Banner';
import { Container } from 'react-bootstrap';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    return (
        <Container fluid>
        <Banner data={data}/>
        </Container>
    )   
}