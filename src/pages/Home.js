import React from 'react';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import FeaturedProducts from '../components/FeaturedProducts'
import CurrentTime from '../components/CurrentTime';
import { Container } from 'react-bootstrap';

export default function Home() {


const data = {
    title: "ShopeeZada Online Shop",
    content: "Shop anytime anywhere!",
    destination: "/products",
    label: "Shop now!"
}
	return (
		<>
		<Container fluid>
			  <Banner data={data} />
			  <Highlights />
			  <FeaturedProducts />
			  <CurrentTime />
		</Container>
		</>
		
	)
}