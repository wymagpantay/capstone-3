import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);

  function authenticate(e) {
    e.preventDefault();
    fetch('https://cpstn3-be-ecommerceapi-magpantay.onrender.com/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('Data received from login API:', data);

        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: 'Login successful',
            icon: 'success',
            text: 'Welcome to ShopeeZada!',
          });
        } else {
          Swal.fire({
            title: 'Authentication failed',
            icon: 'error',
            text: 'Check your login details and try again',
          });
        }
      });
    setEmail('');
    setPassword('');
  }

  const retrieveUserDetails = (token) => {
    fetch('https://cpstn3-be-ecommerceapi-magpantay.onrender.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('User details received:', data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  }

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    user.id !== null ? (
      <Navigate to="/products" />
    ) : (
      <Container fluid>
        <div className="fullview">
        <div className="login-form-container">
        <div className="login-form">
        <Form onSubmit={(e) => authenticate(e)}>
          <h1 className="p-3 text-center">Login</h1>
          
          <Form.Group controlId="userEmail">
            <Form.Label className="login-form-font">Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label className="login-form-font">Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>

          <div className="login-button-container">
          {isActive ? (
            <Button className = "login-button" type="submit" id="submitBtn">
              Login
            </Button>
          ) : (
            <Button className = "login-button" type="submit" id="submitBtn" disabled>
              Login
            </Button>
          )}
          </div>
        </Form>
        </div>
        </div>
        </div>
      </Container>
    )
  );
}