import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId } = useParams();

  const [product, setProduct] = useState(null);
  const [selectedQuantity, setSelectedQuantity] = useState(1);
  const [currentProductQuantity, setCurrentProductQuantity] = useState(null);

  useEffect(() => {
    fetch(`https://cpstn3-be-ecommerceapi-magpantay.onrender.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
        setCurrentProductQuantity(data.quantity);
      });
  }, [productId]);

  const handleQuantityChange = (e) => {
    setSelectedQuantity(parseInt(e.target.value, 10));
  };

  const addToCart = async () => {
    try {
      const response = await fetch(`https://cpstn3-be-ecommerceapi-magpantay.onrender.com/carts/add`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({
          productId: productId,
          quantity: selectedQuantity,
        }),
      });

      if (response.ok) {
        const responseData = await response.json();
        Swal.fire({
          title: 'Successfully added to cart',
          icon: 'success',
          text: 'The product has been added to your cart.',
        });
        
        setProduct((prevProduct) => {
          const updatedProduct = { ...prevProduct };
          updatedProduct.quantity -= selectedQuantity;
          return updatedProduct;
        });
      } else {
      }
    } catch (error) {
    }
  };

  return (
    <Container fluid>
    <div className="fullview">
      {product && (
        <Row>
          <Col lg={{ span: 6, offset: 3 }}>
            <Card.Body className="text-center">
              <Card.Title className="my-4"><span className="product-view-font">{product.name}</span></Card.Title>
              <Card.Text className="my-4"><span className="product-view-black-font">{product.description}</span></Card.Text>
              <Card.Subtitle>Price</Card.Subtitle>
              <Card.Text ><span className="product-view-black-font">P{product.price}</span></Card.Text>
              <Card.Subtitle >Current Quantity</Card.Subtitle>
              <Card.Text><span className="product-view-black-font">{currentProductQuantity}</span></Card.Text>
              <Card.Subtitle>Quantity</Card.Subtitle>
              <Card.Text>
                <input
                  type="number"
                  value={selectedQuantity}
                  onChange={handleQuantityChange}
                  min="1"
                  max={product.quantity}
                  className="product-view-black-font"
                />
              </Card.Text>
              <Button className = "register-button" block onClick={addToCart}>
                Add to Cart
              </Button>
            </Card.Body>
          </Col>
        </Row>
      )}
      </div>
    </Container>
  );
}