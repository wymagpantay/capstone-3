import {useEffect, useState, useContext} from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import { Container } from 'react-bootstrap';


export default function Products() {

	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);

	const fetchData = () => {
		fetch(`https://cpstn3-be-ecommerceapi-magpantay.onrender.com/products/all`)
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			setProducts(data);
		});
	}
	useEffect(() =>{
		fetchData();
	}, []);

	return(
		<>
		    {
		    	(user.isAdmin === true) 
		    		?
		    		<AdminView productsData={products} fetchData={fetchData}/>
		    		:
		    		<UserView productsData={products} />
			}	
		</>
	)
};