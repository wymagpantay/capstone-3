import React, { useContext, useState, useEffect } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useNavigate, Navigate } from 'react-router-dom';
import ResetPassword from '../components/ResetPassword';
import UpdateProfile from '../components/UpdateProfile';
import OrderView from '../components/OrderView';

export default function Profile() {
  const { user } = useContext(UserContext);

  const [details, setDetails] = useState({});
  const [showUpdateProfileForm, setShowUpdateProfileForm] = useState(false);
  const [showResetPasswordForm, setShowResetPasswordForm] = useState(false);
  const [showOrderView, setShowOrderView] = useState(false);

  useEffect(() => {
    fetch(`https://cpstn3-be-ecommerceapi-magpantay.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data._id !== undefined) {
          setDetails(data);
        }
      })
      .catch((error) => {
        console.error('Fetch error:', error);
      });
  }, []);

  return (
    <>
      {details.id === null ? (
        <Navigate to="/products" />
      ) : (
        <Container fluid>
        <div className="fullview">
          <div className="profile-container">
            <div className='pt-5 profile-form'>
              <Row>
                <Col className="p-5">
                  <h1 className="p-3 text-center">My Profile</h1>
                  <h2 className="mt-3 profile-form-font">Welcome back <span className="bold-text-profile">{`${details.firstName} ${details.lastName}`}</span>!</h2>
                  <hr className="divider" />
                  <h4>Details</h4>
                  <ul>
                    <li className="profile-details-font">Email: {`${details.email}`}</li>
                    <li className="profile-details-font">Mobile No: {`${details.mobileNo}`}</li>
                  </ul>
                </Col>
              </Row>
              <Row className="pt-4">
                <Col>
                  <button
                    onClick={() => setShowResetPasswordForm(!showResetPasswordForm)}
                    className = "search-button my-3"
                  >
                    Reset Password
                  </button>
                  {showResetPasswordForm && <ResetPassword />}
                </Col>
              </Row>
              <Row className='pt-4'>
                <Col>
                  <button
                    onClick={() => setShowUpdateProfileForm(!showUpdateProfileForm)}
                    className = "search-button my-3"
                  >
                    Update Profile
                  </button>
                  {showUpdateProfileForm && <UpdateProfile />}
                </Col>
              </Row>
              <Row className="pt-4">
                  <Col>
                    <button
                      onClick={() => setShowOrderView(!showOrderView)}
                      className="search-button my-3"
                    >
                      My Order History
                    </button>
                    {showOrderView && <OrderView />}
                  </Col>
                </Row>
            </div>
          </div>
          </div>
        </Container>
      )}
    </>
  );
}