import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import '../App.css';

export default function Register() {
  const { user } = useContext(UserContext);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    fetch('https://cpstn3-be-ecommerceapi-magpantay.onrender.com/users/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setFirstName('');
          setLastName('');
          setEmail('');
          setMobileNo('');
          setPassword('');
          setConfirmPassword('');

          
          Swal.fire({
            title: 'Registration Successful',
            icon: 'success',
            text: 'Thank you for registering!',
          });
        } else {
          
          Swal.fire({
            title: 'Registration Error',
            icon: 'error',
            text: 'Registration failed. Please try again.',
          });
        }
      })
      .catch((error) => {
        console.error('An error occurred during registration', error);
      });
  }

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      mobileNo !== '' &&
      password !== '' &&
      confirmPassword !== '' &&
      password === confirmPassword &&
      mobileNo.length === 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
    <Container fluid>
      <div className="fullview">
        <div className="register-form-container">
          <div className="register-form">
            <Form onSubmit={(e) => registerUser(e)}>
              <h1 className="p-3 text-center">Register</h1>

              <Form.Group>
                <Form.Label className="register-form-font">First Name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter First Name"
                  required
                  value={firstName}
                  onChange={(e) => {
                    setFirstName(e.target.value);
                  }}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label className="register-form-font">Last Name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Last Name"
                  required
                  value={lastName}
                  onChange={(e) => {
                    setLastName(e.target.value);
                  }}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label className="register-form-font">Email:</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter Email"
                  required
                  value={email}
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label className="register-form-font">Mobile No:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter 11-digit No."
                  required
                  value={mobileNo}
                  onChange={(e) => {
                    setMobileNo(e.target.value);
                  }}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label className="register-form-font">Password: </Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter Password"
                  required
                  value={password}
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label className="register-form-font">Confirm Password: </Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  required
                  value={confirmPassword}
                  onChange={(e) => {
                    setConfirmPassword(e.target.value);
                  }}
                />
              </Form.Group>
              <div className="register-button-container">
                {isActive ? (
                  <Button className="register-button" type="submit" id="submitBtn">
                    Submit
                  </Button>
                ) : (
                  <Button className="register-button" id="submitBtn" disabled>
                    Submit
                  </Button>
                )}
              </div>
            </Form>
          </div>
        </div>
      </div>
    </Container>
  );
}